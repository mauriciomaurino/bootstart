module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		meta: {
			banner:
			'/*!\n' +
			'* <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
			'<%= grunt.template.today("yyyy-mm-dd") %>\n' +
			'<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
			'* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %> (<%= pkg.author.url %>)\n' +
			'* Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %>\n*/\n'
		},
		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'src/css',
					src: ['*.scss'],
					dest: 'src/css',
					ext: '.css',
					extDot: 'last'
				}]
			}
		},
		concat: {
			options: {
				separator: '\n',
				stripBanner: true
			},
			styles: {
				src: ['src/css/*.css'],
				dest: 'css/main.css'
			},
			scripts: {
				src: ['src/js/*.js'],
				dest: 'js/main.js'
			}
		},
		autoprefixer: {
			options: {
				browsers: ['last 3 versions', '> 8%']
			},
			dist: {
				files: [{
					expand: true,
					cwd: 'css/',
					src: '{,*/}*.css',
					dest: 'css/'
				}]
			}
		},
		uglify: {
			options: {
				banner: '<%= meta.banner %>'
			},
			scripts: {
				files: {
					'js/main.min.js': ['js/main.js']
				}
			}
		},
		cssmin: {
			style: {
				src:  'css/main.css',
				dest: 'css/main.min.css'
			}
		},
		copy: {
			styles: {
				cwd: 'src/css/imports',
				src: [ '*.css' ],
				dest: 'css',
				expand: true
			},
			scripts: {
				cwd: 'src/js/imports',
				src: [ '**' ],
				dest: 'js',
				expand: true
			},
		},
		clean: {
			files: []
		},
		imagemin: {
			build: {
				files: [{
					expand: true,
					cwd: 'img',
					src: '{,*/}*.{png,jpg,jpeg,gif}',
					dest: 'img'
				}]
			}
		},
		watch: {
			options: {
				livereload: true
			},
			templates: {
				files: ['**/*.{html,php}', '!vendor/*', '!bootstrap/*', '!node_modules/*', '!_*/*'],
				tasks: []
			},
			styles: {
				files: ['src/css/*.*css'],
				tasks: ['sass', 'concat', 'autoprefixer', 'cssmin', 'copy']
			},
			scripts: {
				files: ['src/js/*.js'],
				tasks: ['concat', 'uglify', 'copy']
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-sass');
	
	grunt.registerTask('default', ['sass', 'concat']);
	grunt.registerTask('build', ['sass', 'concat', 'autoprefixer', 'uglify', 'copy', 'cssmin', 'clean', 'imagemin']);
};